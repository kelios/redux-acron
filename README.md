# redux-acron
This libs its a helper to avoid the re-write of actions, reducers, actions creators and watchers for a redux-saga flow. 

## Table of Contents
1. [Instalation](#instalation)
1. [Usage](#usage)
    * [Actions Types](#types)
    * [Actions Creators](#creators)
    * [State](#state)
    * [Reducer](#reducer)
    * [Saga](#saga)
1. [Example](#example)
1. [Contributros](#contributos)
1. [Special Thanks](#thanks)

## Requirements
* node `^5.0.0`
* yarn `^0.23.0` or npm `^3.0.0`

## Installation
```bash
$ yarn add redux-acroin
```

## Usage