import * as acron from '../src/index'
import { call, put, } from 'redux-saga/effects'

console.log(acron)

const module = acron.createModule({
  state: {
    count: 0,
    asyncCount: 0,
  },
  name: 'myTest',
  handlers: {
    incrementCount: (state, { payload, }) => {
      return ({
      ...state,
      count: state.count + payload 
      })
    },

    asyncIncrement: (state, { payload, status='pending', }) => {
      console.log('asyncIncrement', payload, status)
      if (status === 'pending') {
        console.log('Pending async request....')
      }

      if (status === 'success') {
        console.log('Success request')
        return {
          ...state,
          asyncCount: state.asyncCount + payload.count
        }
      }

      if (status === 'failed') {

      }

      return state
    },
  },
  effects: {
    asyncIncrement: {
      type: 'latest', //every
      *callback(actions, { payload, }) {
        console.log('Delay started')
        yield call(setTimeout, () => {},3000)
        console.log('Delay finished')
        yield put(actions.asyncIncrement({ count: payload.count }, 'success'))
      }
    }
  }
})

export default module
