import { combineReducers } from 'redux'
import createStore from './store'
import config from './config'

export default () => {
  return createStore({}, combineReducers(config.reducers), config.rootSagas)
}
