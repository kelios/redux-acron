import 'babel-polyfill'
import { createModule, } from '../src/index'
import example from '../example'
import config from '../example/config'


describe('Module Test', () => {
  it('should create a model', () => {
    let store = example()

    console.log('Initial State', store.getState())
    store.dispatch(config.actions.myTest.incrementCount(2))
    console.log('State', store.getState())

    store.dispatch(config.actions.myTest.asyncIncrement({ count: 1 }))
    
    console.log('State', store.getState())
  })
})
